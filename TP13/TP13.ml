(* Question 6 *)

(* Structure unir et trouver *)
type graphe = {
  mutable vertexes : (int*int) array;
  links : int array;
}


(* [0,2,2,3] *)
(* (0) (1)->(2) (3) *)

let create_graphe (edges_Number:int) v =
  let l = Array.make edges_Number 0 in
  for i = 1 to edges_Number - 1 do
    l.(i) <- i
  done;
  let g = {vertexes = v; links = l}
  in g

exception OperationImpossible

let rec find g value =
  if g.links.(value) = value then value
  else find g (g.links.(value))

let edges_of_vertexes g vert =
  let s1,s2 = g.vertexes.(vert) in
  (find g s1),(find g s2)

(** On unit s1 à s2  *)
let unir g vertexe_to_merge =
  let n = Array.length g.vertexes in
  if vertexe_to_merge >= n then raise OperationImpossible
  else
    let s1,s2 = edges_of_vertexes g vertexe_to_merge in
    g.links.(s1) <- s2

let supprime_boucle g =
  let l = ref [] in
  for i = 0 to Array.length g.vertexes - 1 do
    let s1,s2 = g.vertexes.(i) in
    if s1 <> s2 then l := g.vertexes.(i) :: !l
  done;
 g.vertexes <- Array.of_list !l

let coupe g =
  for i = 0 to Array.length g.links - 2 do
    unir g (Random.int (Array.length g.links))
  done;
