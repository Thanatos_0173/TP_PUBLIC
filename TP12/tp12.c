#include <math.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

int est_premier_naif(int64_t n)
{
    if (n == 1 || n == 0) { return 0; }
    for (int i = 2; i * i <= n; i++) {
        if ((n % i) == 0) { return 0; }
    }
    return 1;
}

int* crible_eratosthene(int64_t N)
{
    int* arr = malloc((N + 1) * sizeof(int));
    arr[0] = 0;
    arr[1] = 0;
    int compteur = 2;
    while (compteur <= N) {
        for (int i = compteur; i <= N; i++) {
            if (compteur != i && i % compteur == 0) { arr[i] = 1; }
        }
        if (arr[compteur] != 1) { arr[compteur] = 0; }
        compteur++;
    }
    return arr;
}

int main()
{
    int* t = crible_eratosthene(10);
    for (int i = 0; i < 11; i++) { printf("%d ", i); }
    printf("\n");
    for (int i = 0; i < 11; i++) { printf("%d ", t[i]); }
    free(t);
    return 0;
}
