type automate = {n : int ; t : bool array ; delta :  (int*char*int) list };;

type exprat =
    | Lettre of char
    | Lettre_n of char*int (* pour l'expression linéarisée *)
    | Union of exprat * exprat
    | Concat of exprat * exprat
    | Etoile of exprat ;;


let regEx = Concat
    (
      Etoile(Union(Concat(Lettre 'a',Lettre 'b'),Lettre 'b')),
      Concat(Lettre 'b',Lettre 'a')
    )

let regEx2 = Concat
    (
      Concat(Lettre 'b',Lettre 'a'),
      Etoile(Union(Concat(Lettre 'a',Lettre 'b'),Lettre 'b'))
    )

let lineariser (regex:exprat) compt =
  let finCompt = ref compt in
  let rec lineariserRegEx regex =  match regex with
  | Lettre(regex1) | Lettre_n(regex1,_) -> finCompt := !finCompt + 1;Lettre_n(regex1,!finCompt-1)
  | Union(regex1,regex2) ->  Union(lineariserRegEx regex1 ,lineariserRegEx regex2 )
  | Concat(regex1,regex2) -> Concat(lineariserRegEx regex1 ,lineariserRegEx regex2 )
  | Etoile(regex1) ->        Etoile(lineariserRegEx regex1 )
  in
  let regEx = lineariserRegEx regex in
  regEx,!finCompt

let rec regExToString (regex:exprat) = match regex with
  | Lettre(regex1) -> String.make 1 regex1
  | Lettre_n(regex1,value) -> (String.make 1 regex1)^string_of_int value
  | Union(regex1,regex2) -> "("^(regExToString regex1) ^ "|" ^ (regExToString regex2)^")"
  | Concat(regex1,regex2) -> (regExToString regex1) ^ (regExToString regex2)
  | Etoile(regex1) -> "("^(regExToString regex1) ^")"^ "*"

let rec printRegex (regex:exprat) = print_endline (regExToString regex)



let motvide = function
  | Etoile(_) -> true
  | Union(Lettre ' ',_) | Union(_,Lettre ' ') -> true
  | _ -> false

let rec union l1 l2 = match l1,l2 with
  | [],[] -> []
  | [],h::t | h::t,[] -> h::t
  | h::t,h2::t2 -> if h < h2 then h::(union t (h2::t2)) else h2::(union (h::t) t2)


let rec prefixe = function
  | Lettre(regex1) | Lettre_n(regex1,_) -> [Lettre regex1]
  | Concat(regex1,regex2) -> prefixe regex1
  | Union(regex1,regex2) -> prefixe regex1 @ prefixe regex2
  | Etoile(regex1) -> prefixe regex1

let rec suffixe = function
  | Lettre(regex1) | Lettre_n(regex1,_) -> [Lettre regex1]
  | Concat(regex1,regex2) -> suffixe regex2
  | Union(regex1,regex2) -> suffixe regex1 @ suffixe regex2
  | Etoile(regex1) -> suffixe regex1

let rec produitAux elem l = match l with
  | [] -> []
  | h::t -> (elem,h) :: produitAux elem t

let rec produit l1 l2 = match l1 with
  | [] -> []
  | h::t -> (produitAux h l2) @ produit t l2

let rec facteur = function
  (* Les cas de base *)
  | Lettre(_) | Lettre_n(_,_) -> []
  | Concat(Lettre(char1),Lettre(char2)) -> [Lettre(char1),Lettre(char2)]
  | Union (Lettre(char1),Lettre(char2)) -> []
  (*L'induction*)
  | Concat(regex1,regex2) -> union (union (facteur regex1) (facteur regex2)) (produit (suffixe regex1) (prefixe regex2))
  | Union(regex1,regex2) -> union (facteur regex1) (facteur regex2)
  | Etoile(regex1) -> union (facteur regex1) (produit (suffixe regex1) (prefixe regex1))

let terminaux (regex:exprat) n =
