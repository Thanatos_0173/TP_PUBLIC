#include "clock.h"
#include <math.h>
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>

/* Exercice 2 */

pthread_mutex_t LOCK;

struct thread_args {
    double *arr;
    int len;
    double *result;
};

typedef struct thread_args thread_args;

double *create_array(int len)
{
    double *arr = malloc(len * sizeof(double));
    for (int i = 0; i < len; i++) { arr[i] = sin(i); }
    // for (int i = 0; i < len; i++) { arr[i] = i; }
    return arr;
}
/* Question 1 */
void printArray(double *array, int len)
{
    printf("[");
    for (int i = 0; i < len; i++) {
        printf("%f", array[i]);
        if (i < len - 1) { printf(","); }
    }
    printf("]\n");
}

void printArguments(thread_args array)
{
    printf("{\n");
    printf("  {\n");
    printf("    Array : ");
    printArray(array.arr, array.len);
    printf("    Length : %d\n", array.len);
    printf("    Result : %f\n", *array.result);
    printf("  }");
    printf("}\n");
}

void *partial_sum_1(void *funargs)
{
    thread_args *args = (thread_args *)funargs;
    for (int i = 0; i < args->len; i++) { *args->result += args->arr[i]; }

    return NULL;
}

void createThreads1(int n, int cuttingSize)
{
    double *array = create_array(n);
    thread_args *args_array = malloc((n / cuttingSize) * sizeof(thread_args));
    pthread_t *threads = malloc((n / cuttingSize) * sizeof(pthread_t));

    for (int i = 0; i < n / cuttingSize; i++) {
        args_array[i].len = cuttingSize;
        args_array[i].arr = &(array[cuttingSize * i]);
        args_array[i].result = malloc(sizeof(double));
        *args_array[i].result = 0;
    }

    for (int i = 0; i < n / cuttingSize; i++) {
        pthread_create(&threads[i], NULL, partial_sum_1, &args_array[i]);
    }
    for (int i = 0; i < n / cuttingSize; i++) {
        pthread_join(threads[i], NULL);
    }
    double result = 0;
    for (int i = 0; i < n / cuttingSize; i++) {
        result += *args_array[i].result;
        free(args_array[i].result);
    }
    printf("%f\n", result);
    free(args_array);
    free(array);
    free(threads);
}

void *partial_sum_2(void *funargs)
{
    thread_args *args = (thread_args *)funargs;
    pthread_mutex_lock(&LOCK);
    for (int i = 0; i < args->len; i++) { *args->result += args->arr[i]; }
    pthread_mutex_unlock(&LOCK);
    return NULL;
}

void createThreads2(int n, int cuttingSize)
{

    pthread_mutex_init(&LOCK, NULL);
    // int n = 10;
    // int cuttingSize = 2;

    double *array = create_array(n);

    thread_args *args_array = malloc((n / cuttingSize) * sizeof(thread_args));
    pthread_t *threads = malloc((n / cuttingSize) * sizeof(pthread_t));

    for (int i = 0; i < n / cuttingSize; i++) {
        args_array[i].len = cuttingSize;
        args_array[i].arr = &(array[cuttingSize * i]);
        args_array[i].result = malloc(sizeof(double));
        *args_array[i].result = 0;
    }

    for (int i = 0; i < n / cuttingSize; i++) {
        pthread_create(&threads[i], NULL, partial_sum_2, &args_array[i]);
    }

    for (int i = 0; i < n / cuttingSize; i++) {
        pthread_join(threads[i], NULL);
    }
    double result = 0;
    for (int i = 0; i < n / cuttingSize; i++) {
        result += *args_array[i].result;
        free(args_array[i].result);
    }
    printf("%f\n", result);

    free(args_array);
    free(array);
    free(threads);
}

/* Question 3 */

void *partial_sum_3(void *funargs)
{
    thread_args *args = (thread_args *)funargs;
    pthread_mutex_lock(&LOCK);
    double wait = 0;
    for (int i = 0; i < args->len; i++) { wait += args->arr[i]; }
    *args->result += wait;
    pthread_mutex_unlock(&LOCK);
    return NULL;
}

void createThreads3(int n, int cuttingSize)
{

    pthread_mutex_init(&LOCK, NULL);
    // int n = 10;
    // int cuttingSize = 2;

    double *array = create_array(n);

    thread_args *args_array = malloc((n / cuttingSize) * sizeof(thread_args));
    pthread_t *threads = malloc((n / cuttingSize) * sizeof(pthread_t));

    for (int i = 0; i < n / cuttingSize; i++) {
        args_array[i].len = cuttingSize;
        args_array[i].arr = &(array[cuttingSize * i]);
        args_array[i].result = malloc(sizeof(double));
        *args_array[i].result = 0;
    }

    for (int i = 0; i < n / cuttingSize; i++) {
        pthread_create(&threads[i], NULL, partial_sum_3, &args_array[i]);
    }

    for (int i = 0; i < n / cuttingSize; i++) {
        pthread_join(threads[i], NULL);
    }
    double result = 0;
    for (int i = 0; i < n / cuttingSize; i++) {
        result += *args_array[i].result;
        free(args_array[i].result);
    }
    printf("%f\n", result);

    free(args_array);
    free(array);
    free(threads);
}

int main(int argc, char *argv[])
{
    int n;
    int cuttingSize;
    printf("Size of the array : ");
    scanf("%d", &n);
    printf(
        "Number of cuts/Thread number (if the result of the division isn't an "
        "pure integer, "
        "unexpected things may happen, like a bad sommation or errors due to "
        "the code trying to access a non existant value of the array. ) : ");
    scanf("%d", &cuttingSize);
    printf("First Method:\n");
    createThreads1(n, cuttingSize);
    printf("Second Method:\n");
    createThreads2(n, cuttingSize);
    printf("Third Method:\n");
    createThreads3(n, cuttingSize);

    return 0;
}
