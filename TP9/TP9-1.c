#include "clock.h"
#include <math.h>
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>

/* Exercice 1 */

void *showMessage(void *args)
{
    printf("Hewwo World\n");
    return NULL;
}

void createNThreads()
{
    int tNumber;
    printf("Threads numbers: ");
    scanf("%d", &tNumber);
    pthread_t *threads = malloc(tNumber * sizeof(pthread_t));
    for (int i = 0; i < tNumber; i++) {
        pthread_create(&threads[i], NULL, showMessage, NULL);
    }
    for (int i = 0; i < tNumber; i++) { pthread_join(threads[i], NULL); }

    free(threads);
}

/* Exercice 2 */

double SUM = 0.;
pthread_mutex_t LOCK;

struct thread_args {
    double *arr;
    int len;
};

typedef struct thread_args thread_args;

double *create_array(int len)
{
    double *arr = malloc(len * sizeof(double));
    for (int i = 0; i < len; i++) { arr[i] = sin(i); }
    // for (int i = 0; i < len; i++) { arr[i] = i; }
    return arr;
}
/* Question 1 */
void printArray(double *array, int len)
{
    printf("[");
    for (int i = 0; i < len; i++) {
        printf("%f", array[i]);
        if (i < len - 1) { printf(","); }
    }
    printf("]\n");
}

void naiveSum(double *array, int len)
{
    double a = 0;
    for (int i = 0; i < len; i++) { a += array[i]; }
    printf("%f\n", a);
}
void *partial_sum_1(void *funargs)
{
    thread_args *args = (thread_args *)funargs;
    for (int i = 0; i < args->len; i++) { SUM += args->arr[i]; }
    return NULL;
}

void createThreads1(int n, int cuttingSize)
{
    // int n = 10;
    // int cuttingSize = 2;

    double *array = create_array(n);

    thread_args *args_array = malloc((n / cuttingSize) * sizeof(thread_args));
    pthread_t *threads = malloc((n / cuttingSize) * sizeof(pthread_t));

    for (int i = 0; i < n / cuttingSize; i++) {
        args_array[i].len = cuttingSize;
        args_array[i].arr = &(array[cuttingSize * i]);
    }

    for (int i = 0; i < n / cuttingSize; i++) {
        pthread_create(&threads[i], NULL, partial_sum_1, &args_array[i]);
    }
    for (int i = 0; i < n / cuttingSize; i++) {
        pthread_join(threads[i], NULL);
    }
    free(args_array);
    free(array);
    free(threads);
}

void *partial_sum_2(void *funargs)
{
    thread_args *args = (thread_args *)funargs;
    pthread_mutex_lock(&LOCK);
    for (int i = 0; i < args->len; i++) { SUM += args->arr[i]; }
    pthread_mutex_unlock(&LOCK);
    return NULL;
}

void createThreads2(int n, int cuttingSize)
{

    pthread_mutex_init(&LOCK, NULL);
    // int n = 10;
    // int cuttingSize = 2;

    double *array = create_array(n);

    thread_args *args_array = malloc((n / cuttingSize) * sizeof(thread_args));
    pthread_t *threads = malloc((n / cuttingSize) * sizeof(pthread_t));

    for (int i = 0; i < n / cuttingSize; i++) {
        args_array[i].len = cuttingSize;
        args_array[i].arr = &(array[cuttingSize * i]);
    }

    for (int i = 0; i < n / cuttingSize; i++) {
        pthread_create(&threads[i], NULL, partial_sum_2, &args_array[i]);
    }

    for (int i = 0; i < n / cuttingSize; i++) {
        pthread_join(threads[i], NULL);
    }

    free(args_array);
    free(array);
    free(threads);
}

/* Question 3 */

void *partial_sum_3(void *funargs)
{
    thread_args *args = (thread_args *)funargs;
    pthread_mutex_lock(&LOCK);
    double wait = 0;
    for (int i = 0; i < args->len; i++) { wait += args->arr[i]; }
    SUM += wait;
    pthread_mutex_unlock(&LOCK);
    return NULL;
}

void createThreads3(int n, int cuttingSize)
{

    pthread_mutex_init(&LOCK, NULL);
    // int n = 10;
    // int cuttingSize = 2;

    double *array = create_array(n);

    thread_args *args_array = malloc((n / cuttingSize) * sizeof(thread_args));
    pthread_t *threads = malloc((n / cuttingSize) * sizeof(pthread_t));

    for (int i = 0; i < n / cuttingSize; i++) {
        args_array[i].len = cuttingSize;
        args_array[i].arr = &(array[cuttingSize * i]);
    }

    for (int i = 0; i < n / cuttingSize; i++) {
        pthread_create(&threads[i], NULL, partial_sum_3, &args_array[i]);
    }

    for (int i = 0; i < n / cuttingSize; i++) {
        pthread_join(threads[i], NULL);
    }

    free(args_array);
    free(array);
    free(threads);
}

int main(int argc, char *argv[])
{
    int n;
    int cuttingSize;
    printf("Size of the array : ");
    scanf("%d", &n);
    printf(
        "Number of cuts/Thread number (if the result of the division isn't an "
        "pure integer, "
        "unexpected things may happen, like a bad sommation or errors due to "
        "the code trying to access a non existant value of the array. ) : ");
    scanf("%d", &cuttingSize);
    createThreads1(n, cuttingSize);
    printf("%f\n", SUM);
    return 0;
}
