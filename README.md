# TP

Cette branche recense tous les TP d'Informatique 2023-2024 depuis le TP5, les autres TPs ayant étés perdus (des rumeurs disent qu'un mauvais `rm -rf` a été exécuté...).

Index :
| TP         | Sujet                                                                                 | Notes                                                                                                             |
|------------|---------------------------------------------------------------------------------------|-------------------------------------------------------------------------------------------------------------------|
| TP5        | Algorithme MINMAX                                                                     |                                                                                                                   |
| TP6        | Commandes GREP                                                                        |                                                                                                                   |
| TP7        | Automates                                                                             |                                                                                                                   |
| TP8        | Automates                                                                             |                                                                                                                   |
| TP9-1      | Threading : Before Exercice 2, Question 2                                             |                                                                                                                   |
| TP9-2      | Threading : After Exercice 2, Question 2                                              |                                                                                                                   |
| TP10-[1,6] | Threading : Le diner des philosophes, X correspond à la question sur la feuille du TD |                                                                                                                   |
| TP10-Q7    | Implémentation d'un TRI RAPIDE avec des threads                                       |                                                                                                                   |
| TP11       | Réfutation : Calcul de NCF                                                            |                                                                                                                   |
| TP12       | Primalité des nombres entiers                                                         |                                                                                                                   |
| TP13       | Min-Cut randomisé                                                                     |                                                                                                                   |
| TP14       | Bin Packing                                                                           |                                                                                                                   |
| TP15       | Je sais plus mais pas ouf                                                             |                                                                                                                   |
| TP16       | Grammaires                                                                            |                                                                                                                   |
| TP17       | Grammaires - Dérivation                                                               |                                                                                                                   |
| TP18       | k-moyennes                                                                            | Pour utiliser, faire `.\executable <nom_du_fichier_source.ppm> <nom_du_fichier_à_créer.ppm> <nombre_de_couleurs>` |
|            |                                                                                       |                                                                                                                   |

# Pour Thanatos :

Nothing to show...
