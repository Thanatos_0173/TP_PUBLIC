type formule =
  | Var of int
  | Neg of formule
  | Et of formule * formule
  | Ou of formule * formule
  | Implique of formule * formule

type litteral = int
type clause = int list  (* triée *)

let f1 = Implique (Implique (Var 1, Var 2), Var 3)

let f2 = Ou (Implique(Neg (Var 1), Neg (Var 2) ), Implique (Var 3, Var 1))

let f3  n =
  let a = ref (Et(Var 1, Var 2)) in
  for i = 2 to n do
    a := (Ou (!a, Et (Var i,Var (i+1))))
   done;
   !a

let rec taille formule = match formule with
  | Var _ -> 1
  | Neg f -> 1 + taille f
  | Et (f1,f2)
  | Ou (f1,f2)
  | Implique (f1,f2) -> 1 + (max (taille f1)  (taille f2))


let rec sans_implique = function
  | Var a -> Var a
  | Neg f -> Neg (sans_implique f)
  | Et (f1,f2) -> Et ((sans_implique f1),(sans_implique f2))
  | Ou (f1,f2) -> Ou ((sans_implique f1),(sans_implique f2))
  | Implique (f1,f2) -> Ou ( (sans_implique (Neg f1)), sans_implique f2)

let rec descendre_non = function
  | Var a -> Var a
  | Et (f1,f2) -> Et ((descendre_non f1),(descendre_non f2))
  | Ou (f1,f2) -> Ou ((descendre_non f1),(descendre_non f2))
  | Implique (f1,f2) -> Implique (descendre_non f1, descendre_non f2)
  | Neg f -> begin match f with
    | Var a -> Neg (Var a)
    | Neg f -> descendre_non f
    | Et (f,g) -> Ou((descendre_non (Neg f)), (descendre_non (Neg g)))
    | Ou (f,g) -> Et((descendre_non (Neg f)), (descendre_non (Neg g)))
    | Implique (f,g) -> failwith "Je sais pas quoi faire mais normalement il n'y a plus d'implications"
  end




(* Étape 3 : on fait remonter le connecteur `et` et descendre le connecteur `ou` *)
let rec descendre_ou = function
  | Ou (f1, f2) -> begin
      match descendre_ou f1, descendre_ou f2 with
      | f1, Et (f2, f3) | Et (f2, f3), f1 ->
         descendre_ou (Et (Ou (f1, f2), Ou (f1, f3)))
      | f1, f2 -> Ou (f1, f2)
    end
  | Et (f1, f2) -> Et (descendre_ou f1, descendre_ou f2)
  | f -> f


let fnc f=
  let f1 = sans_implique f in
  let f2 = descendre_non f1 in
  let ret = descendre_ou f2 in
  ret

let est_litteral = function
  | Var _ | Neg (Var _) -> true
  | _ -> false

let rec est_clause = function
  | Ou (f1, f2) ->
     (est_litteral f1 || est_clause f1) && (est_litteral f2 || est_clause f2)
  | f -> est_litteral f

let rec est_fnc = function
  | Et (f1, f2) -> est_fnc f1 && est_fnc f2
  | f -> est_clause f

let rec nb_clauses = function
  | Et (f1, f2) -> nb_clauses f1 + nb_clauses f2
  | _ -> 1
