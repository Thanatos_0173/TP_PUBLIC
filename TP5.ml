(* les couleurs des jetons                                 *)

type couleur = Rouge | Jaune

(* les dimensions du plateau de jeu                        *)
let nb_colonnes = 7
let nb_lignes   = 6

(* le plateau des jeux, de dimension nb_lignes * nb_colonnes *)
type game = couleur option array array

(* un état du jeu                                          *)
type etat = { game : game ; joueur : couleur }

let autre = function Rouge -> Jaune | Jaune -> Rouge

(* Quelques fonctions graphiques *)
(* let color_of_couleur = function Rouge -> "O" | _ -> "X" *)
let red = "\027[31m" ;;
let yellow = "\027[93m" ;;
let neutre = "\027[m" ;;



let color_of_couleur = function Rouge -> red ^ "O" ^ neutre | _ -> yellow ^ "X" ^ neutre;;

let draw_game (e: etat) =
  if e.game = [||] then failwith "Cannot print an empty game";
  for i = nb_lignes -1  downto 0   do
    print_string("| ");
    for j = 0 to nb_colonnes-1   do
        match e.game.(i).(j) with
      | None -> print_string("  ");
      | Some c -> print_string(color_of_couleur c);print_string(" ");
    done;
    print_string("|\n");
  done;
  print_string("  ");
  for j = 0 to nb_colonnes-1   do
    print_int(j);print_string(" ")
  done;
  print_string("  \n");
;;


(* Boite à outils *)
(* d encode une direction : E, NE, N, NO, O, SO, S, SE
                            0   1  2   3, 4,  5, 6,  7
   on calcule la case obtenue en se déplaçant k fois dans la direction d, depuis la case (i, j)
*)
let rec move (k: int) (i, j) (d: int) =
  match d with
  | 0 -> (i+k, j)               (* E *)
  | 1 -> (i+k, j+k)             (* NE *)
  | 2 -> (i  , j+k)             (* N *)
  | 3 -> (i-k, j+k)             (* NO *)
  | _ -> move (-k) (i, j) (d-4)
;;
(* permet de tester si une case (i, j) est dans le plateau de jeu *)
let is_in (i, j) =
  i >= 0 && j >= 0 && j < nb_colonnes && i < nb_lignes;;
(* permet de tester si une case (i, j) du plateau g est libre *)
let is_free g (i, j) =
  is_in (i, j) && g.(i).(j) = None;;
(* permet de tester si une case (i, j) du plateau g contient la couleur c *)
let is_color g (i, j) c =
  is_in (i, j) && g.(i).(j) = Some c;;


(* retourne une copie de la matrice mm *)
let copy_matrix mm =
  let n = Array.length mm in
  if n = 0 then [||]
  else
    let m = Array.length mm.(0) in
    if m = 0 then Array.init n (fun i -> [||])
    else
      Array.init n (fun i -> Array.copy mm.(i))
;;
(* matrice des valeurs des cases *)
let val_cases =
  [|
    [|3 ; 4 ; 5  ; 7  ; 5  ; 4 ; 3|];
    [|4 ; 6 ; 8  ; 10 ; 8  ; 6 ; 4|];
    [|5 ; 8 ; 11 ; 13 ; 11 ; 8 ; 5|];
    [|5 ; 8 ; 11 ; 13 ; 11 ; 8 ; 5|];
    [|4 ; 6 ; 8  ; 10 ; 8  ; 6 ; 4|];
    [|3 ; 4 ; 5  ; 7  ; 5  ; 4 ; 3|];
  |]

(* fonctions à compléter, elles sont définies ici pour pouvoir faire typer le reste du programme *)
let print_option f_to_convert = function | None -> print_string "None" | Some x -> print_string "Some"; print_string (f_to_convert x) ;;

let color_to_string = function | Rouge -> "Rouge" | Jaune -> "Jaune" ;;

let place_jeton (e: etat) (i: int): etat option =
  let etatToReturn = ref None in
  let copy = { game = (copy_matrix e.game); joueur = autre e.joueur} in (*On change le joueur, du coup on appellera e.joueur pour placer le jeton du bon joueur*)
  let canPlay = ref true in
  for l = 0 to 5 do
    (* print_option color_to_string e.game.(l).(i); *)
    (* print_string (string_of_bool (is_free e.game (l,i))); *)
    if is_free copy.game (l,i) && !canPlay then begin
      copy.game.(l).(i) <- Some e.joueur;
      canPlay := false;
      etatToReturn := Some copy;
      (* print_string "Played here" *)
      end
  done;
  !etatToReturn
;;

let etat_test = {game=Array.make_matrix nb_lignes nb_colonnes None; joueur = Rouge};;
etat_test.game.(0).(3) <- Some Rouge;
etat_test.game.(1).(3) <- Some Jaune;
etat_test.game.(0).(2) <- Some Rouge;
etat_test.game.(0).(0) <- Some Jaune;;
(* draw_game etat_test *)

(* attend que le joueur renvoie une colonne et retourne l'état du jeu après *)
let rec read_player_move (e: etat) =
  print_string("Dans quelle colonne voulez_vous jouez?\n");
  let i = read_int() in
  match place_jeton e i with
  | None -> read_player_move e
  | Some(g') -> g'
;;

(* let is_player_winning (e:etat) (c:couleur) : bool = *)
(*   let ret = ref false in *)
(*   let case_same_color = ref 0 in *)
(*   for i = 0 to 5 do *)
(*     for j = 1 to 6 do *)
(*       if e.game.(i).(j) = Some c && !case_same_color = 3 then ret := true *)
(*       else if e.game.(i).(j) = Some c && !case_same_color <> 3 then case_same_color := !case_same_color + 1 *)
(*       else case_same_color := 0 (\*On the line, there is not 4 cases lined up and with the same color*\) *)
(*     done; *)
(*   done; *)
(*   if not !ret then begin (\*On pas pas trouvé de cases identiques*\) *)
(*     case_same_color := 0; *)
(*     for j = 0 to 6 do *)
(*       for i = 0 to 5 do *)
(*         if e.game.(i).(j) = Some c && !case_same_color = 3 then ret := true *)
(*         else if e.game.(i).(j) = Some c && !case_same_color <> 3 then case_same_color := !case_same_color + 1 *)
(*         else case_same_color := 0 (\*On the line, there is not 4 cases lined up and with the same color*\) *)
(*       done; *)
(*     done *)
(*   end; *)
(*   if not !ret then begin (\*On pas pas trouvé de cases identiques*\) *)
(*     for i = 0 to 5 do *)
(*       for j = 0 to 6 do *)
(*         (\*Diagonales de gauche à droites*\) *)
(*         if is_in (i,j) && is_in (i+1,j+1) && is_in (i+2,j+2) && is_in (i+3,j+3) then (\*On va tous forcer, donc il faut vérifier que tout soit bien dans le tableau*\) *)
(*           if e.game.(i).(j) = Some c && e.game.(i+1).(j+1) = Some c && e.game.(i+2).(j+2) = Some c && e.game.(i+3).(j+3) = Some c then ret := true *)
(*       done; *)
(*     done *)
(*   end; *)
(*   if not !ret then begin *)
(*     for i = 5 downto 0 do *)
(*       for j = 6 downto 0 do *)
(*         (\*Diagonales de gauche à droites*\) *)
(*         if is_in (i,j) && is_in (i-1,j-1) && is_in (i-2,j-2) && is_in (i-3,j-3) then (\*On va tous forcer, donc il faut vérifier que tout soit bien dans le tableau*\) *)
(*           if e.game.(i).(j) = Some c && e.game.(i-1).(j-1) = Some c && e.game.(i-2).(j-2) = Some c && e.game.(i-3).(j-3) = Some c then ret := true *)
(*       done; *)
(*     done *)
(*   end; *)
(*   !ret *)

let is_player_winning (e:etat) (c:couleur) =
  let b = ref false in
  for i = 0 to nb_lignes - 1 do
    for j = 0 to nb_colonnes - 1 do
      for direction = 0 to 3 do
        let aligne = ref true in
        for h = 0 to 3 do
          let new_c = move h (i,j) direction in
          aligne := !aligne && is_in new_c && is_color e.game new_c c
        done;
        b := !b || !aligne
      done;
    done;
  done;
  !b
;;

let who_win e = if is_player_winning e Rouge then Rouge else Jaune ;;

let heuristique (e: etat) (c: couleur) : int =
  if is_player_winning e c then max_int
  else if is_player_winning e (autre c) then min_int
  else
    (*No player is winning*)
    let h = ref 0 in
    for i = 0 to nb_lignes - 1  do
      for j = 0 to nb_colonnes -1 do
        if e.game.(i).(j) = Some c then h := !h +  val_cases.(i).(j)
        else if e.game.(i).(j) = Some (autre c) then h := !h -  val_cases.(i).(j)
      done;
    done;
  !h
;;

let addToRef (e_opt : etat option) (ref : etat list ref) = match e_opt with
  | None -> ()
  | Some c -> ref := c :: !ref
;;

let next (e:etat) : etat list =
  if is_player_winning e Rouge || is_player_winning e Jaune then []
  else
  let lToReturn = ref [] in
  for i = 0 to nb_colonnes - 1 do addToRef (place_jeton e i) lToReturn done;
  !lToReturn
;;

let rec minmax (p:int) (e:etat) (c:couleur) =
  let hash : (etat, int) Hashtbl.t = Hashtbl.create 10000 in
  match next e, p with
  | [],_ -> heuristique e c
  | _,0  -> heuristique e c (*Should not be empty*)
  | l,_ -> let f, deb = if c = e.joueur then max, ref (-100) else min, ref 100 in
    let h successeur =
      let v = if Hashtbl.mem hash successeur then Hashtbl.find hash successeur
        else begin
          let result = minmax (p-1) successeur (autre c) in
          Hashtbl.add hash successeur result;
          result
        end
      in
      deb := f !deb v
    in List.iter h l;
    !deb
;;


let draw_game_and_heur p e =
  draw_game e;
  print_string "Heuristique :";
  print_int (minmax p e e.joueur)

let print_array_state array =
  for i = 0 to Array.length array -1 do
    print_int i;print_string "_____________________________________________________________________";
    print_newline();
    List.iter draw_game array.(i)
  done
;;

let rec joue (e:etat) (p:int) =
  draw_game e;
  let all = Array.make (p+1) [] in
  all.(0) <- [e];
  let ret = ref { game = [||]; joueur = Rouge } in
  let rec retunSuccessOfAllList list = match list with
    | [] -> []
    | h::t -> List.append (next h) (retunSuccessOfAllList t)
  in
  for i = 0 to p-1 do
    all.(i+1) <- retunSuccessOfAllList all.(i)
  done;
  let max = ref min_int in
  let check etat =
    let value = minmax p etat e.joueur in
    if value > !max then begin
      max := value;
      ret := etat
  end
  in List.iter check (all.(p-1));
  !ret
;;



(* let main () = *)
(*   (\* définition du jeu initial *\) *)
(*   let init = { game = Array.make_matrix nb_lignes nb_colonnes None ; joueur = Rouge } in *)
(*   let rec play_with_user (e: etat) = *)
(*     draw_game e; *)
(*     (\* le joueur joue *\) *)
(*     let e' = read_player_move e in *)
(*     draw_game e'; *)
(*     (\* l'ordinateur joue *\) *)
(*     let e'' = joue e' 5 in *)
(*     play_with_user e'' *)
(*   in *)
(*   play_with_user init *)
