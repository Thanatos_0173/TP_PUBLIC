#include <ctype.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
typedef struct {
    int r;
    int g;
    int b;
} rgb;

/* Question 2 */
typedef struct {
    int x;
    int y;
    rgb** image;
} Image;
typedef struct {
    int x;
    int y;
} coord;

void printRgb(rgb* c) { printf("%d %d %d\n", c->r, c->g, c->b); }
Image* open(char* filename)
{
    FILE* file = fopen(filename, "r");
    if (file == NULL) {
        printf("Empty File or file not found");
        fclose(file);
        return NULL;
    }
    char* st = malloc(1000);
    int isFirstLine = 1;
    int isSecondLine = 1;
    Image* image = malloc(sizeof(Image));
    int rgbc = 0;
    rgb* color = malloc(sizeof(rgb));
    rgb** colorList;
    int x = 0;
    int y = 0;
    while (fscanf(file, "%[^\n]\n", st) == 1) {
        if (st[0] != '#' && st[0] != 'P' && st[0] != '\0') {
            if (isFirstLine) {
                int i = 0;
                // Tokenize the string
                int a[2]; // Array to store integers
                char* token = strtok(st, " ");
                while (token != NULL && i < 2) {
                    a[i] = atoi(token);
                    i++;
                    token = strtok(NULL, " ");
                }
                image->x = a[0];
                image->y = a[1];
                isFirstLine = 0;
                colorList = malloc(image->y * sizeof(rgb*));
            }
            else if (!isFirstLine && isSecondLine) {
                isSecondLine = 0;
            }
            else {
                if (rgbc % 3 == 0) { color->r = atoi(st); }
                if (rgbc % 3 == 1) { color->g = atoi(st); }
                if (rgbc % 3 == 2) {
                    color->b = atoi(st);
                    // rgbc is only increasing, but we can find x and y like this:
                    // x = i % width
                    // y = i / width
                    if (x == 0) { colorList[y] = malloc(image->x * sizeof(rgb)); }
                    colorList[y][x] = *color;
                    ++x;
                    x %= image->x;
                    if (x == 0) // We are starting a new line
                    {
                        y++;
                    }
                }
                rgbc++;
            }
        }
    }
    free(st);
    free(color);
    image->image = colorList;
    fclose(file);
    return image;
}
void create(char* filename, Image* image)
{
    FILE* file = fopen(filename, "w");
    if (file == NULL) {
        printf("Failed to create file");
        return;
    }
    fputs("P3\n#Made by Thanatos\n", file);
    int sizeOfNumberOne = (int)((ceil(log10(image->x))));
    int sizeOfNumberTwo = (int)((ceil(log10(image->y))));
    char* result = malloc((sizeOfNumberOne + sizeOfNumberTwo + 3) * sizeof(char));
    /*     printf("%d", (sizeOfNumberOne + sizeOfNumberTwo + 1)); */
    sprintf(result, "%d %d\n", image->x, image->y);
    fputs(result, file);
    fputs("255\n", file);
    for (int i = 0; i < image->x * image->y; i++) {
        int x = i % image->x;
        int y = i / image->x;
        rgb k = image->image[y][x];
        for (int _ = 0; _ < 3; _++) {
            int n = (_ == 0) ? k.r : (_ == 1 ? k.g : k.b);
            int length = (n != 0) ? floor(log10(abs(n))) + 1 : 1;
            char* s = malloc((length + 2) * sizeof(char));
            sprintf(s, "%d\n", n);
            fputs(s, file);
            free(s);
        }
    }
    fclose(file);
    free(result);
    return;
}

void mirror(char* filename, Image* image)
{
    rgb** newImage = malloc((image->x * image->y) * sizeof(rgb));
    for (int i = 0; i < image->x * image->y; i++) {
        int x = i % image->x;
        int y = i / image->x;
        if (x == 0) { newImage[y] = malloc(image->x * sizeof(rgb)); }
        newImage[y][x] = image->image[y][image->x - x - 1];
    }
    for (int i = 0; i < image->y; i++) { free(image->image[i]); }
    free(image->image);
    image->image = newImage;
    create(filename, image);
}

int distance(rgb* color1, rgb* color2) { return pow(color1->r - color2->r, 2) + pow(color1->g - color2->g, 2) + pow(color1->b - color2->b, 2); }

void randomCentersArray(coord* array, int size, Image* image, rgb* colorOfCenters)
{
    for (int i = 0; i < size; i++) {
        int x = rand() % image->x;
        int y = rand() % image->y;
        array[i].x = x;
        array[i].y = y;
        colorOfCenters[i] = image->image[y][x];
    }
    return;
}

int nearest(rgb* pixel, coord* centers, int centersNumber, Image* image)
{
    int dist = distance(pixel, &image->image[centers[0].y][centers[0].x]);
    int ret = 0;
    for (int i = 1; i < centersNumber; i++) {
        if (dist > distance(pixel, &image->image[centers[i].y][centers[i].x])) {
            dist = distance(pixel, &image->image[centers[i].y][centers[i].x]);
            ret = i;
        }
    }
    return ret;
}

void reloadCenters(Image* image, int** cluster, int k, coord* centers)
{
    int* nb = malloc(k * sizeof(int));
    for (int i = 0; i < k; i++) {
        centers[i].x = 0;
        centers[i].y = 0;
        nb[i] = 0;
    }
    for (int i = 0; i < image->y; i++) {
        for (int j = 0; j < image->x; j++) {
            centers[cluster[i][j]].x += j;
            centers[cluster[i][j]].y += i;
            nb[cluster[i][j]]++;
        }
    }
    for (int i = 0; i < k; i++) {
        if (nb[i] != 0) {
            centers[i].y /= nb[i];
            centers[i].x /= nb[i];
        }
    }
    free(nb);
}

void bubbleSort(coord* arr, int n)
{
    int i, j;
    for (i = 0; i < n - 1; i++) {
        // Last i elements are already in place
        for (j = 0; j < n - i - 1; j++) {
            // Swap if the current element is greater than the next one
            if (arr[j].x > arr[j + 1].x || (arr[j].x == arr[j + 1].x && arr[j].y > arr[j + 1].y)) {
                coord temp = arr[j];
                arr[j] = arr[j + 1];
                arr[j + 1] = temp;
            }
        }
    }
}

int isEqual(coord* cluster1, coord* cluster2, int k)
{
    bubbleSort(cluster1, k);
    bubbleSort(cluster2, k);
    for (int i = 0; i < k; i++) {
        if (cluster1[i].x != cluster2[i].x || cluster1[i].y != cluster2[i].y) { return 0; }
    }
    return 1;
}

void replaceImage(Image* image, int** cluster, rgb* colorsOfCenters)
{
    for (int i = 0; i < image->y; i++) {
        for (int j = 0; j < image->x; j++) { image->image[i][j] = colorsOfCenters[cluster[i][j]]; }
    }
}

void compressImage(Image* im, int k)
{
    coord* centers = malloc(k * sizeof(coord));
    coord* oldCenters = malloc(k * sizeof(coord));
    rgb* colorOfCenters = malloc(k * sizeof(rgb));
    int** cluster = malloc(im->y * sizeof(int*));
    int it = 0;
    printf("Initialising everything.\n");
    // Initiation of the clusters
    for (int i = 0; i < im->y; i++) {
        cluster[i] = malloc(im->x * sizeof(int));
        for (int j = 0; j < im->x; j++) { cluster[i][j] = 0; }
    }
    // We create k random classes
    randomCentersArray(centers, k, im, colorOfCenters);
    // For each pixel, we give it the appropriate cluster
    for (int i = 0; i < k; i++) { oldCenters[i] = centers[i]; }
    printf("Started to calculate the Image. It might take some time.");
    while (!isEqual(centers, oldCenters, k) || it == 0) {
        it++;
        for (int i = 0; i < im->y; i++) {
            for (int j = 0; j < im->x; j++) { cluster[i][j] = nearest(&im->image[i][j], centers, k, im); }
        }
        // We reload the centers
        for (int i = 0; i < k; i++) { oldCenters[i] = centers[i]; }
        reloadCenters(im, cluster, k, centers);
        // We replace the image with the simplified colors
        replaceImage(im, cluster, colorOfCenters);
        // we save the image
    }
    printf("\nFinished");
    free(centers);
    free(oldCenters);
    free(colorOfCenters);
    for (int i = 0; i < im->y; i++) { free(cluster[i]); }
    free(cluster);
}

int main(int argc, char* argv[])
{
    srand(time(NULL));
    Image* im = open(argv[1]);
    compressImage(im, atoi(argv[3]));
    create(argv[2], im);
    for (int i = 0; i < im->y; i++) { free(im->image[i]); }

    free(im->image);
    free(im);
    return 0;
}
