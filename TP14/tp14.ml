(* TP14 *)

type instance = int * int list

(* Question 5 *)

type box =
  {
   mutable i : int;
   mutable occupied : int;
    element : int array;
  }

let print_box b = for i = 0 to Array.length b.element -1 do print_int b.element.(i); print_string " " done;;

let next_fit (inst: instance) : box list =
  let rec aux (inst: instance) (current: box) = match inst with
    | size, [] -> [current]
    | size, h::t -> begin
        let disponible = size - (current.occupied) in
        if disponible < h then
          current::(aux (size,h::t) {i = 0; occupied = 0; element = Array.make size 0})
        else
          begin
            current.element.(current.i) <- h;
            current.i <- current.i + 1;
            current.occupied <- current.occupied + h;
            aux (size,t) current
          end
        end
      in
      let size,_ = inst in
      aux inst {i = 0; occupied = 0; element = Array.make size 0};;

(* next_fit (10,[2;5;4;7;1;3;8]);; *)
(* Question 7 *)

let first_fit (inst: instance) : box list =
  let rec aux (inst:instance ) (bList : box list) = match inst with
    | size, [] -> bList
    | size, h::t -> begin
        let rec throughAllBoxes (size:int) (element:int) = function
          | [] -> begin let arr = Array.make size 0 in arr.(0) <- h; [{i = 1; occupied = h; element = arr}] end
          | boite::t -> begin
              let disponible = size - (boite.occupied) in
              if disponible < h then
                boite :: throughAllBoxes (size) (h) t
              else begin
                boite.element.(boite.i) <- h;
                boite.i <- boite.i + 1;
                boite.occupied <- boite.occupied + h;
                boite :: t
              end
            end
        in
       aux (size,t) (throughAllBoxes size h bList)
      end
    in
    aux inst [];;

let first_fit_desc (inst:instance) : box list =
  let size,l = inst in
  let m = List.sort (fun x y -> compare y x) l in
  first_fit (size,m);;

first_fit_desc (10,[2;5;4;7;1;3;8]);;
