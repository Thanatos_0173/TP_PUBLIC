type afd = {
  nb_etats : int;
  finaux : bool array;
  transition : (int * char * int) list;
};;

let a = {nb_etats = 1; finaux = [|true|]; transition = [(0,'a',0)]};;
let vide = {nb_etats = 2; finaux = [|false;true|]; transition = [(0,' ',1)]};;
let langage_vide = {nb_etats = 1; finaux = [|true|]; transition = [(0,' ',0)]};;


let testCool = {nb_etats = 4; finaux = [|false;false;true;true|]; transition = [(0,'a',1);(0,'b',2);(1,'b',0);(1,'a',2)]};;


exception Blocage;;

let rec printCharList list = match list with [] -> () | h::t -> print_char h; print_char ' '; printCharList t

let rec execute automate u =
  let rec transition etat (lettre:char) transitions =
    match transitions with
    | [] -> raise Blocage
    | (init,char,fin)::t -> if init = etat && char = lettre then fin
                            else transition etat lettre t
  in
  let rec deltastar etat mot = match mot with
    | [] -> raise Blocage
    | [l] -> transition etat l automate.transition
    | h::t -> deltastar (transition etat h automate.transition) t
  in
  deltastar 0 u

let rec reconnait automate mot =
  let exceptionCatched = ref false in
  let aux toChange automate mot =
    try
      if execute automate mot >= 0 then toChange := false
    with
    | Blocage -> toChange := true;
  in
  aux exceptionCatched automate mot;
  (not !exceptionCatched)

(* Complexité de reconnait :
   La compexité de reconnait de dépends que de la complexité d'execute :
   Execute fait |u| appels à transition, qui fait elle même au pire |E| appels. La complexité au pire est donc :
   O(|u||E|)
*)
(*
  Pour une implémentation plus efficace des automates, il faudrait que l'algorithme n'ai pas à parcourir toute la liste pour récupérer les transitions possibles
*)

type afd_effi = {
  nb_etats : int;
  finaux : bool array;
  transition : (int * char * int) list array;
  (*
     Le premier indicage correspond à l'état : toutes les fonctions de transition partant de létat 0 sont stockées en [0], etc.
  *)


}

let pasEffToEff (automate:afd) =
  let (toRet:afd_effi) = {nb_etats = automate.nb_etats; finaux = automate.finaux;
               transition = Array.make automate.nb_etats [] ;} in
  let rec addE list = match list with
    | [] -> ()
    | (init,etat,fin)::t ->  toRet.transition.(init)  <- (init, etat, fin) :: toRet.transition.(init); addE t
  in
  addE automate.transition;
  toRet

let effToPasEff automate =

  let list = ref [] in
  let rec add = function
    | [] -> ()
    | (init,etat,fin)::t -> list  := (init, etat, fin) :: !(list); add t
  in
  for i = 0 to automate.nb_etats - 1 do add automate.transition.(i) done;

  let (toRet:afd) = {nb_etats = automate.nb_etats; finaux = automate.finaux;
                          transition = !list ;} in
  toRet


let rec executeBetter automate u =
  let rec transitionBetter (lettre:char) transitions =
    match transitions  with
    | [] -> raise Blocage
    | (_,char,fin)::t -> if char = lettre then fin
      else transitionBetter lettre t
  in
  let rec deltastarBetter etat mot = match mot with
    | [] -> raise Blocage
    | [l] -> transitionBetter l automate.transition.(etat)
    | h::t -> deltastarBetter (transitionBetter h automate.transition.(etat)) t
  in
  deltastarBetter 0 u

let rec reconnaitBetter automate mot =
  let exceptionCatched = ref false in
  let aux toChange automate mot =
    try
      if executeBetter automate mot >= 0 then toChange := false
    with
    | Blocage -> toChange := true;
  in
  aux exceptionCatched automate mot;
  (not !exceptionCatched)


exception NotComplete
let rec existTransition etat lettre transitions =
      match transitions with
      | [] -> false
      | (init,char,fin)::t -> if init = etat && char = lettre then true
                              else existTransition etat lettre t

let est_complet alphabet (automate:afd) =
    let toReturn = ref true in
    let rec aux alphabet =
      match alphabet with
      | [] -> !toReturn
      | h::t ->
        for i = 0 to automate.nb_etats-1 do
          if not (existTransition i h automate.transition ) then toReturn := false;
        done;
        aux t
    in
    aux alphabet

let complete alphabet (automate:afd) =
  let new_finaux = Array.make (automate.nb_etats + 1) false in
  for i = 0 to (automate.nb_etats - 1) do
    new_finaux.(i) <- automate.finaux.(i)
  done;
  let liste = ref automate.transition in

  let rec addToListe alphabet = match alphabet with
    | [] -> ()
    | h::t ->
      for i = 0 to (automate.nb_etats - 1) do
        print_int i;
        print_newline();
        print_string (string_of_bool(existTransition i h automate.transition));
        if not (existTransition i h automate.transition) then begin
          liste := (i,h,automate.nb_etats)::!liste
          end
      done;
      addToListe t
  in
  addToListe alphabet;
  let (newAutomate:afd) = {nb_etats = automate.nb_etats + 1; finaux = new_finaux; transition = !liste}
  in newAutomate



let rec print_list = function | [] -> print_string "()" | h::t -> print_int h; print_char '|'; print_list t

let rec allTransitionsFromState seened acc automate etat = function
  | [] -> acc
  | (init,_,fin)::t -> if init = etat && not seened.(fin) then begin
      seened.(init) <- true;
      allTransitionsFromState seened (fin::acc) automate etat t
    end
    else allTransitionsFromState seened acc automate etat t

let rec parcours (automate:afd) etatInitial seen =
  seen.(etatInitial) <- true;
  let rec callOnTransitions = function
    | [] -> ()
    | h::t ->
      if not seen.(h) then begin
        parcours automate h seen;
        callOnTransitions t;
      end
  in
  callOnTransitions (allTransitionsFromState (Array.make (automate.nb_etats) false) [] automate etatInitial automate.transition)

let accessibles (automate:afd) =
  let n = (automate.nb_etats)  in
  let seen = Array.make n false in
  parcours automate 0 seen;
  seen

let coaccessibles (automate:afd) =
  let reverseAutomate (automate:afd) =
    let liste = ref [] in
    let rec reverseListe = function | [] -> () | (init,char,fin)::t -> liste := (fin,char,init) :: !liste; reverseListe t in
    reverseListe automate.transition;
    let (newAutomate:afd) = {nb_etats = automate.nb_etats; finaux = automate.finaux; transition = !liste} in newAutomate
  in
  let newAutomate = reverseAutomate automate in
  let n = (newAutomate.nb_etats)  in
  let seen = Array.make n false in
  parcours newAutomate 0 seen;
  seen

let renumeroter etats =
  let toRet = Array.make (Array.length etats) (-1) in
  let s = ref 0 in
  for i = 0 to Array.length etats - 1 do
    if etats.(i) then toRet.(i) <- !s; s := !s + 1
  done;
  toRet

(* Pour l'émonder, on le complète *)



type afnd = {
  nb_etats : int;
  initiaux : bool array;
  finaux : bool array;
  transition : (int * char * int) list;
};;                            (* ON CONSIDÈRE QUE LE MOT VIDE EST LE CARACTÈRE ' '*)


let (test1ND:afnd) = {nb_etats = 4;
              initiaux = [|true;false;false;false|];
              finaux = [|false;false;false;true|];
              transition = [(0,'a',0);(0,'a',1);(0,'b',2);(0,'b',3);
                            (1,'b',3);
                            (2,'a',3);
                            (3,'b',1)
                           ]}

let estNonDetermine automate =
    let rec loop i count =
      if i = Array.length automate.initiaux then true
      else
        if automate.initiaux.(i) then begin
           if count > 0 then false
           else loop (i+1) (count + 1)
        end
        else
          loop (i+1) count
    in
    if not (loop 0 0) then true
    else
      let alreadySin = Array.make_matrix (automate.nb_etats) 255 false in (* Because 255 is the max ascii table size *)
      let rec checkAllTransition = function
        | [] -> false
        | (_,' ',_)::t -> true
        | (a,b,_)::t ->
          if not alreadySin.(a).(int_of_char b) then begin
            alreadySin.(a).(int_of_char b) <- true;
            checkAllTransition t
          end
          else false
      in
      checkAllTransition automate.transition;
