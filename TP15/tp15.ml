(* Question 1 *)

(* On condidère que le point de départ est TOUJOURS le sommet edges.(0) (S'il n'existe pas, le graphe est vide) *)

let g0 =
  [|
  [|0;5;4;3|];
  [|5;0;2;6|];
  [|4;2;0;12|];
  [|3;6;12;0|]
|]



let bound g nbNonVus =
  let n = ref 0 in
  for i = 0 to Array.length g - 1 do
    for j = 0 to Array.length g.(0) -1 do
      n := max (!n) (g.(i).(j))
    done;
  done;
  (nbNonVus + 1) * !n

let solve g =
  let n = Array.length g in
  let seen = Array.make n false in
  let m = ref max_int in
  let rec aux s g ptot nbNonVus =
    for i = 0 to n - 1 do
      if i <> s && not seen.(i) then
        begin

          if i <> 0 then
            begin
              if ptot + g.(s).(i) > bound g nbNonVus then
                begin
                  seen.(s) <- false;
                  ()
                end
              else
               begin
                 print_int s; print_int nbNonVus;
                 print_newline ();
                 print_int ptot;
                 print_newline ();
                 print_int (bound g (nbNonVus + 1));
                 print_newline ();print_endline "uwu";
                 seen.(s) <- true;
                 aux i g (ptot + g.(s).(i)) (nbNonVus + 1)
               end
            end
          else m := min ptot (!m)
       end
     done
   in aux 0 g 0 0;
   !m
