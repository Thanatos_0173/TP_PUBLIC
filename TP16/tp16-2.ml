(* TP16 *)


type token = IF | THEN | ELSE | PRINT | NUM of int | EQUAL

(* Question 1 *)

type deriv_arbre =
  | EMPTY
  | LEAF of token
  | NODE of int * (deriv_arbre list) (* The int is the applied rule *)

(* Question 2 *)

exception SYNTAX_ERROR of string * token list;;

let rec string_of_token = function
  | NUM a -> string_of_int a
  | EQUAL -> "EQUAL"
  | IF -> "IF"
  | THEN -> "THEN"
  | ELSE -> "ELSE"
  | PRINT -> "PRINT"

let rec print_list = function
  | [] -> ()
  | h::t -> print_string ((string_of_token h)^" "); print_list t

let syntax_analysis (token_list:token list) =
  let list = ref token_list in
  let deriv_e () =
    match !list with
    | NUM a::EQUAL::NUM b::t ->
      begin
        list := t;
        NODE(3,[LEAF (NUM a); LEAF EQUAL; LEAF (NUM b)])
      end
    | _ -> raise (SYNTAX_ERROR ("Bad equal",(!list)))
  in
  let rec deriv_s () = match (!list) with
      | PRINT::NUM a::t -> list := t;NODE(1,[LEAF(PRINT);LEAF(NUM a)])
      | IF::t ->
        begin
          list := List.tl !list;
          let argument_of_if = deriv_e () in (* Now !list should have at the head a THEN *)
          let argument_of_then = ref EMPTY in
          let argument_of_else = ref EMPTY in
          if List.hd !list <> THEN then raise (SYNTAX_ERROR("Bad if", !list));
          list := List.tl !list;
          argument_of_then := deriv_s (); (* Now !list should have at then head a ELSE *)
           if List.hd !list <> ELSE then raise (SYNTAX_ERROR("Bad if", !list));
          list := List.tl !list;
          argument_of_else := deriv_s ();
           NODE(2,LEAF(IF)::argument_of_if :: LEAF(THEN):: !argument_of_then :: LEAF(ELSE):: !argument_of_else :: [])
        end
      | _ -> raise (SYNTAX_ERROR ("Bad printing",(!list)))
  in
  let tree = ref EMPTY in
  tree := deriv_s ();
  if !tree = EMPTY then raise (SYNTAX_ERROR( "Learn how to code",[]))
  else !tree

(* Question 4 *)

exception LEXICAL_ERROR of string * string

let list_of_string string = String.split_on_char ' 'string



let rec token_list_of_string_list = function
  | [] -> []
  | "equal"::t -> EQUAL:: token_list_of_string_list t
  | "if"::t -> IF:: token_list_of_string_list t
  | "then"::t -> THEN:: token_list_of_string_list t
  | "else"::t -> ELSE:: token_list_of_string_list t
  | "print"::t -> PRINT:: token_list_of_string_list t
  | h::t ->
      try (NUM (int_of_string h))::token_list_of_string_list t with | Failure(int_of_string) -> raise(LEXICAL_ERROR("Bad conversion of int to string", h))


let lexical_analysis code =
  token_list_of_string_list (list_of_string code)



let very_difficult_code = "if 4 equal 3 then print 2 else if 2 equal 5 then print 2 else print 9"
let code_two = "if 12 equal 12 then if 5 equal 5 then if 2 equal 2 then if 13 equal 13 then if 8 equal 8 then if 101 equal 101 then if 91 equal 91 then if 24 equal 24 then if 43 equal 43 then if 690 equal 690 then print 1 else print 56 else print 72 else print 47 else print 7 else print 9099 else print 123 else print 321 else print 33 else print 0 else print 25"
let isEqualEqual = function
  | NODE(3,LEAF (NUM a)::LEAF EQUAL :: LEAF(NUM b) :: _) -> a = b
  | l -> failwith ("Not cheking an equal")


let rec evaluation = function
  | NODE(1,_::LEAF token::_) -> print_string (string_of_token token)
  | NODE(2, LEAF IF :: equal :: LEAF THEN :: s :: LEAF ELSE :: s2 :: _) -> if isEqualEqual equal then evaluation s else evaluation s2
  | _ -> failwith "Bad checking"

let intepreter (code:string) = evaluation (syntax_analysis (lexical_analysis code))


(*
RULE 1 : S -> PRINT NUM
RULE 2 : S -> IF E THEN S ELSE S
RULE 3 : E -> NUM EQUAL NUM
*)
