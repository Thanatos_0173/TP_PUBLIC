(* TP16 *)

(*
RULE 1 : S -> PRINT NUM
RULE 2 : S -> IF E THEN S ELSE S
RULE 3 : E -> NUM EQUAL NUM
*)

type token = IF | THEN | ELSE | PRINT | NUM | EQUAL

(* Question 1 *)

type deriv_arbre =
  | EMPTY
  | LEAF of token
  | NODE of int * (deriv_arbre list) (* The int is the applied rule *)

(* Question 2 *)

exception SYNTAX_ERROR of string * token list;;

(* Question 3 *)

(* Liste des erreures de syntaxe :
   -> On fait un PRINT puis plus rien
   -> On fait un EQ mais il n'y a rien aux bornes
*)

let rec string_of_token = function
  | NUM -> "NUM"
  | EQUAL -> "EQUAL"
  | IF -> "IF"
  | THEN -> "THEN"
  | ELSE -> "ELSE"
  | PRINT -> "PRINT"

let rec print_list = function
  | [] -> ()
  | h::t -> print_string ((string_of_token h)^" "); print_list t

let syntax_analysis (token_list:token list) =
  let list = ref token_list in
  (* let rec  = match (List.hd !list)  with *)
  (* in *)
  let deriv_e () =
    match !list with
    | NUM::EQUAL::NUM::t ->
      begin
        list := t;
        NODE(2,[LEAF NUM; LEAF EQUAL; LEAF NUM])
      end
    | _ -> raise (SYNTAX_ERROR ("Bad equal",(!list)))
  in
  let rec deriv_s () = match (!list) with
      | PRINT::NUM::t -> list := t;NODE(1,[LEAF(PRINT);LEAF(NUM)])
      | IF::t ->
        begin
          list := List.tl !list;
          let argument_of_if = deriv_e () in (* Now !list should have at the head a THEN *)
          let argument_of_then = ref EMPTY in
          let argument_of_else = ref EMPTY in
          if List.hd !list <> THEN then raise (SYNTAX_ERROR("Bad if", !list));
          list := List.tl !list;
          argument_of_then := deriv_s (); (* Now !list should have at then head a ELSE *)
           if List.hd !list <> ELSE then raise (SYNTAX_ERROR("Bad if", !list));
          list := List.tl !list;
          argument_of_else := deriv_s ();
           NODE(1,LEAF(IF)::argument_of_if :: LEAF(THEN):: !argument_of_then :: LEAF(ELSE):: !argument_of_else :: [])
        end
      | _ -> raise (SYNTAX_ERROR ("Bad printing",(!list)))
  in
  let tree = ref EMPTY in
  tree := deriv_s ();
  if !tree = EMPTY then raise (SYNTAX_ERROR( "Learn how to code",[]))
  else tree

(* Question 4 *)

exception LEXICAL_ERROR of string * string

let list_of_string string = String.split_on_char ' 'string

let rec token_list_of_string_list = function
  | [] -> []
  | "num"::t -> NUM :: token_list_of_string_list t
  | "equal"::t -> EQUAL:: token_list_of_string_list t
  | "if"::t -> IF:: token_list_of_string_list t
  | "then"::t -> THEN:: token_list_of_string_list t
  | "else"::t -> ELSE:: token_list_of_string_list t
  | "print"::t -> PRINT:: token_list_of_string_list t
  | h::_ -> raise (LEXICAL_ERROR("Learn how to code",h))

let lexical_analysis code =
  token_list_of_string_list (list_of_string code)


(* Question 5 *)

let very_difficult_code = "if num equal num then print num else if num equal num then print num else print num"

(* Question 6 *)
