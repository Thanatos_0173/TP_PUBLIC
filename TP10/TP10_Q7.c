#include "stdio.h"
#include "stdlib.h"
#include "time.h"
#include "unistd.h"
// Premième implémentation : Chaque sous-tableau est calculé dans un thread
// différent.

typedef struct {
    int* head;
    int size;
} array;

typedef struct {
    array* firstElement;
    array* secondElement;
} couple;

void print(array* arr)
{
    printf("[");
    for (int i = 0; i < arr->size; i++) {
        if (i < arr->size - 1) { printf("%d,", arr->head[i]); }
        else {
            printf("%d", arr->head[i]);
        }
    }
    printf("]\n");
}

couple* separateArray(array* arr)
{
    couple* toRet = malloc(sizeof(couple));

    int pivot = arr->size / 2;

    array* arr1 = malloc(sizeof(array));
    array* arr2 = malloc(sizeof(array));

    int* arr1int = malloc(pivot * sizeof(int));
    int* arr2int = malloc((pivot + (arr->size % 2 == 0 ? 0 : 1)) * sizeof(int));

    int iadded = 0;
    int jadded = 0;

    for (int i = 0; i < arr->size; i++) {
        if (arr->head[i] <= pivot) {
            arr1int[iadded] = arr->head[i];
            ++iadded;
        }
        else {
            arr2int[0] = 0;
            jadded++;
        }
    }
    arr1->head = arr->head;
    arr1->size = pivot;

    arr2->head = &arr->head[pivot];
    arr2->size = pivot + (arr->size % 2 == 0 ? 0 : 1);

    toRet->firstElement = arr1;
    toRet->secondElement = arr2;

    return toRet;
}

void freeCouple(couple* couple)
{
    free(couple->firstElement->head);
    free(couple->secondElement->head);

    free(couple->firstElement);
    free(couple->secondElement);
    free(couple);
}

int main()
{

    int* test = malloc(11 * sizeof(int));
    array* arr = malloc(sizeof(array));
    int min = 0;
    int max = 100;
    srand(time(NULL));
    for (int i = 0; i < 11; i++) { test[i] = rand() % (max + i - min) + min; }
    arr->head = &test[0];
    arr->size = 11;

    couple* couple = separateArray(arr);

    print(couple->firstElement);
    print(couple->secondElement);

    print(arr);

    free(test);
    freeCouple(couple);

    free(arr);

    return 0;
}
