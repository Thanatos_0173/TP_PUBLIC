/* TP10 - le squelette de solution */

#include <math.h>
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#define NUM_PHILS 5
#define MAX_BUF   256
#define NUM_BAG   NUM_PHILS

typedef struct {
    int gauche_libre;
    int droite_libre;
    pthread_t thread;
    int prog;
    int prog_total;
    int id;
} philosophe;

pthread_mutex_t baguette[NUM_BAG];
pthread_mutex_t serveur;
philosophe convives[NUM_PHILS];
int stop = 0;
int currentlyEating = 0;
/*
 * Fonction auxilliaires pour attraper les baguettes & referencer les voisins
 */
philosophe *phil_gauche(philosophe *p)
{
    return &convives[(p->id == 0 ? NUM_PHILS - 1 : (p->id) - 1)];
}

philosophe *phil_droit(philosophe *p)
{
    return &convives[(p->id == NUM_PHILS ? 0 : (p->id) + 1)];
}

pthread_mutex_t *bag_gauche(philosophe *p)
{
    return &baguette[(p->id == 0 ? NUM_BAG - 1 : (p->id) - 1)];
}

pthread_mutex_t *bag_droite(philosophe *p) { return &baguette[p->id]; }

/*
 * Code d'un philosophe
 */
static void *phil_thread(void *arg)
{
    int iter = 0;
    int reflechir_rnd, manger_rnd, i;
    philosophe *moi;
    moi = (philosophe *)arg;
    int id = moi->id;

    while (!stop) {
        reflechir_rnd = (rand() % 20);
        manger_rnd = (rand() % 20);
        /*
         * Reflechir pour un temps aleatoire
         */
        usleep(reflechir_rnd);
        if (currentlyEating == NUM_PHILS) { pthread_mutex_lock(&serveur); }
        pthread_mutex_lock(bag_gauche(moi));
        pthread_mutex_lock(bag_droite(moi));
        ++currentlyEating;
        // TODO il faudrait attraper des baguettes ici!

        /*
         * Manger pour un temps aleatoire
         */
        usleep(manger_rnd);

        // TODO il faudrait lacher des baguettes ici!
        pthread_mutex_unlock(bag_droite(moi));
        pthread_mutex_unlock(bag_gauche(moi));
        --currentlyEating;
        if (currentlyEating < NUM_PHILS) { pthread_mutex_unlock(&serveur); }

        /* Mettre a jour le nombre de fois où les philosophes ont mangés */
        moi->prog++;
        moi->prog_total++;
    }

    return NULL;
}

void init_convives()
{
    int i;
    for (i = 0; i < NUM_PHILS; i++) {
        convives[i].gauche_libre = 1;
        convives[i].droite_libre = 1;
        convives[i].prog = 0;
        convives[i].prog_total = 0;
        convives[i].id = i;
    }

    for (i = 0; i < NUM_PHILS; i++) {
        pthread_create(&(convives[i].thread), NULL, phil_thread, &convives[i]);
    }
}

void print_progress()
{
    int i, j;
    char buf[MAX_BUF];

    for (i = 0; i < NUM_PHILS;) {
        for (j = 0; j < 5; j++) {
            if (i == NUM_PHILS) {
                printf("\r");
                break;
            }
            sprintf(
                buf, "p%d=%d/%d", i, convives[i].prog, convives[i].prog_total);
            if (strlen(buf) < 16) { printf("%s\t\t", buf); }
            else {
                printf("%s\t", buf);
            }
            i++;
        }
        if (i == NUM_PHILS) {
            printf("\n");
            break;
        }
        printf("p%d=%d/%d\n", i, convives[i].prog, convives[i].prog_total);
        i++;
    }
}

int main(int argc, char **argv)
{
    int i, deadlock, iter = 0;

    srand(time(NULL));

    for (i = 0; i < NUM_BAG; i++) { pthread_mutex_init(&baguette[i], NULL); }

    pthread_mutex_init(&serveur, NULL);
    init_convives();

    do {
        /*
         * Mettre a zero le progres des philosophes. Si le philosophe fait du
         * progres, le philosophe l'incrementera.
         */
        for (i = 0; i < NUM_PHILS; i++) { convives[i].prog = 0; }

        /*
         * Laisser les philosophes reflechir et manger
         */
        sleep(3);

        /*
         * Verifier si il y a interblockage
         */
        deadlock = 1;
        for (i = 0; i < NUM_PHILS; i++) {
            if (convives[i].prog) { deadlock = 0; }
        }

        /*
         * Afficher le progres des philosophes
         */
        print_progress();
    } while (!deadlock);

    stop = 1;
    printf("Deadlock atteint\n");

    /*
     * Attendre la terminaison des philisophes
     */
    for (i = 0; i < NUM_PHILS; i++) {
        pthread_cancel(convives[i].thread);
        pthread_join(convives[i].thread, NULL);
    }
    return 0;
}
